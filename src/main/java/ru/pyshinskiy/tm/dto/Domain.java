package ru.pyshinskiy.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.ServiceLocator;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement
public class Domain implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement
    @Nullable
    private List<Project> projects;

    @XmlElement
    @Nullable
    private List<Task> tasks;

    @XmlElement
    @Nullable
    private List<User> users;

    @Nullable
    public List<Project> getProjects() {
        return projects;
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    @Nullable
    public List<User> getUsers() {
        return users;
    }

    public void load(ServiceLocator serviceLocator) throws Exception {
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        if(currentUser == null) {
            throw new Exception("Your'e not authorized");
        }
        @NotNull final String userId = currentUser.getId();
        this.projects = new LinkedList<>(serviceLocator.getProjectService().findAll(userId));
        this.tasks = new LinkedList<>(serviceLocator.getTaskService().findAll(userId));
        this.users = new LinkedList<>(serviceLocator.getUserService().findAll());
    }
}
