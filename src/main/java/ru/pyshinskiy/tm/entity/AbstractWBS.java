package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractWBS extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String userId;

    @Nullable
    private Status status = Status.PLANNED;

    @NotNull
    private Date createDate = new Date(System.currentTimeMillis());

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    private String description;
}
