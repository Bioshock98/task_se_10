package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumerated.Role;

@NoArgsConstructor
@Getter
@Setter
public final class
User extends AbstractEntity {

    @Nullable
    private String password;

    @Nullable
    private String login;

    @Nullable
    private Role role;
}
