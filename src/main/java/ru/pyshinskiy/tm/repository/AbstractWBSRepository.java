package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.wbs.IAbstractWBSRepository;
import ru.pyshinskiy.tm.entity.AbstractWBS;
import ru.pyshinskiy.tm.enumerated.Status;

import java.util.*;

public abstract class AbstractWBSRepository<T extends AbstractWBS> extends AbstractRepository<T> implements IAbstractWBSRepository<T> {

    @NotNull final private Map<String, Comparator<T>> comparators  = new LinkedHashMap<>();

    protected AbstractWBSRepository() {
        comparators.put("byCreateTime", Comparator.comparing(AbstractWBS::getCreateDate));

        comparators.put("byStartDate", (o1, o2) -> {
            assert o1.getStartDate() != null;
            assert o2.getStartDate() != null;
            return o1.getStartDate().compareTo(o2.getStartDate());
        });
        comparators.put("byFinishDate", (o1, o2) -> {
            assert o1.getFinishDate() != null;
            assert o2.getFinishDate() != null;
            return o1.getFinishDate().compareTo(o2.getFinishDate());
        });
        comparators.put("byStatus", (o1, o2) -> {
            assert o1.getStatus() != null;
            assert o2.getStatus() != null;
            if (o1.getStatus().equals(o2.getStatus())) return 0;
            else if (o1.getStatus().equals(Status.PLANNED)) return -1;
            else if (o2.getStatus().equals(Status.PLANNED)) return 1;
            else if (o1.getStatus().equals(Status.IN_PROGRESS)) return -1;
            else if (o2.getStatus().equals(Status.IN_PROGRESS)) return 1;
            return 1;
        });
    }

    @NotNull
    public List<T> findAll(@NotNull final String userId) throws Exception {
        @NotNull final LinkedList<T> objectives = new LinkedList<>();
        for(@NotNull final T objective : findAll()) {
            assert objective.getUserId() != null;
            if(objective.getUserId().equals(userId)) objectives.add(objective);
        }
        return objectives;
    }

    @Nullable
    public T findOne(@NotNull final String userId, @NotNull final String id) throws Exception {
        for(@NotNull final T objective : findAll()) {
            assert objective.getId() != null;
            if(objective.getId().equals(id)) return objective;
        }
        return null;
    }

    @Nullable
    public T remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        return entityMap.remove(id);
    }

    public void removeAll(@NotNull final String userId) throws Exception {
        for(@NotNull final T objective : findAll()) {
            assert objective.getUserId() != null;
            if(objective.getUserId().equals(userId)) {
                assert objective.getId() != null;
                remove(objective.getId());
            }
        }
    }

    @Nullable
    @Override
    public List<T> findByName(@NotNull String userId, @NotNull String name) throws Exception {
        @NotNull final List<T> objectives = new LinkedList<>();
        for(@NotNull final T t : findAll(userId)) {
            assert t.getName() != null;
            if(name.equals(t.getName()) || t.getName().contains(name)) objectives.add(t);
        }
        return objectives;
    }

    @Nullable
    @Override
    public List<T> findByDescription(@NotNull String userId, @NotNull String description) throws Exception {
        @NotNull final List<T> objectives = new LinkedList<>();
        for(@NotNull final T t : findAll(userId)) {
            assert t.getDescription() != null;
            if(description.equals(t.getDescription()) || t.getDescription().contains(description)) objectives.add(t);
        }
        return objectives;
    }

    @Override
    @NotNull
    public List<T> sortByCreateTime(@NotNull List<T> vbs, final int direction) throws Exception {
        vbs.sort(comparators.get("byCreateTime"));
        return vbs;
    }

    @Override
    @NotNull
    public List<T> sortByStartDate(@NotNull List<T> vbs, final int direction) throws Exception {
        vbs.sort(comparators.get("byStartDate"));
        return vbs;
    }

    @Override
    @NotNull
    public List<T> sortByFinishDate(@NotNull List<T> vbs, final int direction) throws Exception {
        vbs.sort(comparators.get("byFinishDate"));
        return vbs;
    }

    @Override
    @NotNull
    public List<T> sortByStatus(@NotNull List<T> vbs, final int direction) throws Exception {
        vbs.sort(comparators.get("byStatus"));
        return vbs;
    }
}
