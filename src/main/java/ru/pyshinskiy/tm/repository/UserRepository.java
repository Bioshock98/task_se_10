package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.entity.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable private User currentUser;

    @Override
    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(@Nullable final User user) {
        this.currentUser = user;
    }

    @Override
    @Nullable
    public User getUserByLogin(@NotNull final String login) throws Exception {
        for(@NotNull final User user : findAll()) {
            if(login.equals(user.getLogin())) return user;
        }
        return null;
    }
}
