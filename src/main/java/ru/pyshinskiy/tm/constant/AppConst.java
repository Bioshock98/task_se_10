package ru.pyshinskiy.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public class AppConst {
    @NotNull public final static String SAVE_DIR = System.getProperty("user.dir") + File.separator + "taskmanager" + File.separator;
}
