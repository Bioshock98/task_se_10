package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.Repository;
import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.entity.User;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final IUserRepository userRepository = (IUserRepository) abstractRepository;

    public UserService(@NotNull final Repository<User> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    @Nullable
    public User getCurrentUser() {
        return userRepository.getCurrentUser();
    }

    @Override
    public void setCurrentUser(@Nullable final User user) {
        userRepository.setCurrentUser(user);
    }

    @Override
    @Nullable
    public User getUserByLogin(@Nullable final String login) throws Exception {
        if(login == null) return null;
        return userRepository.getUserByLogin(login);
    }
}
