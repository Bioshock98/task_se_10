package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.Repository;
import ru.pyshinskiy.tm.api.service.Service;
import ru.pyshinskiy.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements Service<T> {

    @NotNull protected final Repository<T> abstractRepository;

    public AbstractService(@NotNull final Repository<T>  abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    @Override
    public T findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return abstractRepository.findOne(id);
    }

    @NotNull
    @Override
    public List<T> findAll() throws Exception {
        return abstractRepository.findAll();
    }

    @Nullable
    @Override
    public T persist(@Nullable final T t) throws Exception {
        if(t == null) return null;
        return abstractRepository.persist(t);
    }

    @Override
    @Nullable
    public List<T> persist(@Nullable final List<T> ts) throws Exception {
        if(ts == null) return null;
        return abstractRepository.persist(ts);
    }

    @Nullable
    @Override
    public T merge(@Nullable final T t) throws Exception {
        if(t == null) return null;
        return abstractRepository.merge(t);
    }

    @Nullable
    @Override
    public T remove(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return abstractRepository.remove(id);
    }

    @Override
    public void removeAll() throws Exception {
        abstractRepository.removeAll();
    }

    @Override
    @Nullable
    public String getIdByNumber(final int number) throws Exception {
        return abstractRepository.getIdByNumber(number);
    }
}
