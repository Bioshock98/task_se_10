package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.wbs.IAbstractWBSRepository;
import ru.pyshinskiy.tm.api.wbs.IAbstractVBSService;
import ru.pyshinskiy.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractWBSService<T extends AbstractEntity> extends AbstractService<T> implements IAbstractVBSService<T> {

    @NotNull protected final IAbstractWBSRepository<T> wbsRepository = (IAbstractWBSRepository<T>) abstractRepository;

    public AbstractWBSService(@NotNull final IAbstractWBSRepository<T> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    @Nullable
    public T findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        return wbsRepository.findOne(userId, id);
    }

    @Override
    @NotNull
    public List<T> findAll(@Nullable final String userId) throws Exception{
        if(userId == null) throw new Exception("userId is null");
        return wbsRepository.findAll(userId);
    }

    @Override
    @Nullable
    public T remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(id == null || id.isEmpty()) throw new Exception();
        return wbsRepository.remove(id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        wbsRepository.remove(userId);
    }

    @Override
    @Nullable
    public List<T> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || name == null) throw new Exception("one of the parameters passed is null");
        return wbsRepository.findByName(userId, name);
    }

    @Override
    @Nullable
    public List<T> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || description == null) throw new Exception("one of the parameters passed is null");
        return wbsRepository.findByDescription(userId, description);
    }

    @NotNull
    public List<T> sortByCreateTime(@NotNull final List<T> tasks, final int direction) throws Exception {
        return wbsRepository.sortByCreateTime(tasks, direction);
    }

    @NotNull
    public List<T> sortByStartDate(@NotNull final List<T> tasks, final int direction) throws Exception {
        return wbsRepository.sortByStartDate(tasks, direction);
    }

    @NotNull
    public List<T> sortByFinishDate(@NotNull final List<T> tasks, final int direction) throws Exception {
        return wbsRepository.sortByFinishDate(tasks, direction);
    }

    @NotNull
    public List<T> sortByStatus(@NotNull final List<T> tasks, final int direction) throws Exception {
        return wbsRepository.sortByStatus(tasks, direction);
    }
}
