package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.entity.Project;

public final class ProjectService extends AbstractWBSService<Project> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository abstractRepository) {
        super(abstractRepository);
    }
}
