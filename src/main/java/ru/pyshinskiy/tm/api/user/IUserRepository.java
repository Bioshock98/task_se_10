package ru.pyshinskiy.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.Repository;
import ru.pyshinskiy.tm.entity.User;

public interface IUserRepository extends Repository<User> {

    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable final User user);

    @Nullable
    User getUserByLogin(@NotNull final String login) throws Exception;
}
