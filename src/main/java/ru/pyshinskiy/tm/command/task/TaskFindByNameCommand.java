package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskFindByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_find_by_name";
    }

    @Override
    @NotNull
    public String description() {
        return "find task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK FIND BY NAME");
        System.out.println("ENTER TASK NAME");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        printTasks(serviceLocator.getTaskService().findByName(userId, name));
        System.out.println("[OK]");
    }
}
