package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;

import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.*;

public final class TaskListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final List<Task> unsortedTasks = taskService.findAll(serviceLocator.getUserService().getCurrentUser().getId());
        @NotNull final List<Task> sortedTasks = new LinkedList<>();
        switch (option) {
            case "createTime" :
                sortedTasks.addAll(taskService.sortByCreateTime(unsortedTasks, 1));
                break;
            case "startDate" :
                sortedTasks.addAll(taskService.sortByCreateTime(unsortedTasks, 1));
                break;
            case "finishDate" :
                sortedTasks.addAll(taskService.sortByCreateTime(unsortedTasks, 1));
                break;
            case "status" :
                sortedTasks.addAll(taskService.sortByCreateTime(unsortedTasks, 1));
        }
        printTasks(sortedTasks);
        System.out.println("[OK]");
    }
}
