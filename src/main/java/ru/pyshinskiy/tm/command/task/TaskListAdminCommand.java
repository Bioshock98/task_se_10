package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.*;

public final class TaskListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(@Nullable final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    @NotNull
    public String command() {
        return "task_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST ADMIN]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final List<Task> unsortedTasks = taskService.findAll();
        @NotNull final List<Task> sortedTasks = new LinkedList<>();
        switch (option) {
            case "createTime" :
                sortedTasks.addAll(taskService.sortByCreateTime(unsortedTasks, 1));
                break;
            case "startDate" :
                sortedTasks.addAll(taskService.sortByStartDate(unsortedTasks, 1));
                break;
            case "finishDate" :
                sortedTasks.addAll(taskService.sortByFinishDate(unsortedTasks, 1));
                break;
            case "status" :
                sortedTasks.addAll(taskService.sortByStatus(unsortedTasks, 1));
        }
        printTasks(sortedTasks);
        System.out.println("[OK]");
    }
}
