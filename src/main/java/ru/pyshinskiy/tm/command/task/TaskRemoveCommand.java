package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll(serviceLocator.getUserService().getCurrentUser().getId()));
        @NotNull final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        taskService.remove(currentUser.getId(), taskId);
        System.out.println("[TASK REMOVED]");
    }
}
