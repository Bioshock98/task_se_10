package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTask;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskSelectCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_select";
    }

    @Override
    @NotNull
    public String description() {
        return "show task info";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("[TASK SELECT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll(currentUser.getId()));
        @NotNull final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        @Nullable final Task task = taskService.findOne(currentUser.getId(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        printTask(task);
        System.out.println("[OK]");
    }
}
