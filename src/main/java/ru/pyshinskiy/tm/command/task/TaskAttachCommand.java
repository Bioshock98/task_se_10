package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskAttachCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_attach";
    }

    @Override
    @NotNull
    public String description() {
        return "attach task to project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("[ATTACH TASK]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll(currentUser.getId()));
        @NotNull final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        @Nullable final Project project = projectService.findOne(currentUser.getId(), projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll(serviceLocator.getUserService().getCurrentUser().getId()));
        @NotNull final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        @Nullable final Task task = taskService.findOne(currentUser.getId(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        task.setProjectId(project.getId());
        taskService.merge(task);
        System.out.println("[OK]");
    }
}
