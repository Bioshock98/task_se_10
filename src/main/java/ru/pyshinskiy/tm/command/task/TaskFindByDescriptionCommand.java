package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskFindByDescriptionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_find_by_description";
    }

    @Override
    @NotNull
    public String description() {
        return "find task by description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK FIND BY DESCRIPTION]");
        System.out.println("ENTER TASK DESCRIPTION");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        printTasks(serviceLocator.getTaskService().findByDescription(userId, description));
        System.out.println("[OK]");
    }
}
