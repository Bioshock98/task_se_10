package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.enumerated.Status;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public class TaskChangeStatusCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_change_status";
    }

    @Override
    @NotNull
    public String description() {
        return "change task status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CHANGE STATUS]");
        System.out.println("ENTER TASK ID");
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        printTasks(serviceLocator.getTaskService().findAll(userId));
        final int taskNumber = Integer.parseInt(serviceLocator.getTerminalService().nextLine()) - 1;
        @NotNull final String taskId = serviceLocator.getTaskService().getIdByNumber(taskNumber);
        @Nullable final Task task = serviceLocator.getTaskService().findOne(userId, taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        System.out.println("Current task status is " + task.getStatus());
        System.out.println("ENTER NEW STATUS");
        @NotNull final Status newStatus = Status.valueOf(serviceLocator.getTerminalService().nextLine());
        task.setStatus(newStatus);
        serviceLocator.getTaskService().merge(task);
        System.out.println("[OK]");
    }
}
