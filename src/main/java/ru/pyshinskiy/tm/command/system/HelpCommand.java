package ru.pyshinskiy.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

public final class HelpCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(@Nullable final User user) {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "help";
    }

    @Override
    @NotNull
    public String description() {
        return "list available commands";
    }

    @Override
    public void execute() {
        for(@NotNull final AbstractCommand command : serviceLocator.getCommands()) {
            if(command.isAllowed(serviceLocator.getUserService().getCurrentUser())) {
                System.out.println(command.command() + ": " +
                        command.description());
            }
        }
        System.out.println("[OK]");
    }
}
