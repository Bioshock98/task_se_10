package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectEditAdminCommand extends AbstractCommand {
    
    @Override
    public boolean isAllowed(@Nullable final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    @NotNull
    public String command() {
        return "project_edit_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "edit any project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        @NotNull final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        @Nullable final Project project = projectService.findOne(projectId);
        @NotNull final Project anotherProject = new Project();
        anotherProject.setUserId(project.getUserId());
        System.out.println("ENTER NAME");
        anotherProject.setName(terminalService.nextLine());
        anotherProject.setId(project.getId());
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherProject.setStartDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("ENTER FINISH DATE");
        anotherProject.setFinishDate(parseDateFromString(terminalService.nextLine()));
        projectService.merge(anotherProject);
        System.out.println("[OK]");
    }
}
