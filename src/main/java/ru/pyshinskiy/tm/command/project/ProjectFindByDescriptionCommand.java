package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public class ProjectFindByDescriptionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_find_by_description";
    }

    @Override
    @NotNull
    public String description() {
        return "find project by its description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND BY DESCRIPTION]");
        System.out.println("ENTER PROJECT DESCRIPTION");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        printProjects(serviceLocator.getProjectService().findByDescription(userId, description));
        System.out.println("[OK]");
    }
}
