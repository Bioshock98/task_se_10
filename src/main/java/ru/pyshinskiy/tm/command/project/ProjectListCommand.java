package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;

import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.*;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, finishDate, status]");
        @NotNull final String option = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        @NotNull final List<Project> unsortedProjects = projectService.findAll(currentUser.getId());
        @NotNull final List<Project> sortedProjects = new LinkedList<>();
        switch (option) {
            case "createTime":
                sortedProjects.addAll(projectService.sortByCreateTime(unsortedProjects, 1));
                break;
            case "startDate":
                sortedProjects.addAll(projectService.sortByStartDate(unsortedProjects, 1));
                break;
            case "finishDate":
                sortedProjects.addAll(projectService.sortByFinishDate(unsortedProjects, 1));
                break;
            case "status":
                sortedProjects.addAll(projectService.sortByStatus(unsortedProjects, 1));
        }
        printProjects(sortedProjects);
        System.out.println("[OK]");
    }
}
