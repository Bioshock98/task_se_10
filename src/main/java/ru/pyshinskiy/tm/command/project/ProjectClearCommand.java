package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_clear";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all projects";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        taskService.removeAttachedTasks(currentUser.getId());
        projectService.removeAll(currentUser.getId());
    }
}
