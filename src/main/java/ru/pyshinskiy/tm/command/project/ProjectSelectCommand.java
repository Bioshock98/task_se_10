package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProject;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectSelectCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_select";
    }

    @Override
    @NotNull
    public String description() {
        return "show project info";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll(currentUser.getId()));
        @NotNull final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        @Nullable final Project project = projectService.findOne(currentUser.getId(), projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        printProject(project);
        System.out.println("[OK]");
    }
}
