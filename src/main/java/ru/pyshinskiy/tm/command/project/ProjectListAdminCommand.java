package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.*;

public final class ProjectListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(@Nullable final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    @NotNull
    public String command() {
        return "project_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, finishDate, status]");
        @NotNull final String option = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final List<Project> unsortedProjects = projectService.findAll();
        @NotNull final List<Project> sortedProjects = new LinkedList<>();
        switch (option) {
            case "createTime" :
                sortedProjects.addAll(projectService.sortByCreateTime(unsortedProjects, 1));
                break;
            case "startDate" :
                sortedProjects.addAll(projectService.sortByStartDate(unsortedProjects, 1));
                break;
            case "finishDate" :
                sortedProjects.addAll(projectService.sortByFinishDate(unsortedProjects, 1));
                break;
            case "status" :
                sortedProjects.addAll(projectService.sortByStatus(unsortedProjects, 1));
        }
        printProjects(sortedProjects);
        System.out.println("[OK]");
    }
}
