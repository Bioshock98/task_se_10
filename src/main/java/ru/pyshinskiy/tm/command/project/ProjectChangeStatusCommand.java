package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.enumerated.Status;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public class ProjectChangeStatusCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_change_status";
    }

    @Override
    @NotNull
    public String description() {
        return "change project status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CHANGE STATUS]");
        System.out.println("ENTER PROJECT ID");
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        printProjects(serviceLocator.getProjectService().findAll(userId));
        @NotNull final int projectNumber = Integer.parseInt(serviceLocator.getTerminalService().nextLine()) - 1;
        @NotNull final String projectId = serviceLocator.getProjectService().getIdByNumber(projectNumber);
        @Nullable final Project project = serviceLocator.getProjectService().findOne(userId, projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        System.out.println("Current project status is " + project.getStatus());
        System.out.println("ENTER NEW STATUS");
        @NotNull final Status newStatus = Status.valueOf(serviceLocator.getTerminalService().nextLine());
        project.setStatus(newStatus);
        serviceLocator.getProjectService().merge(project);
        System.out.println("[OK]");
    }
}
