package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public class ProjectFindByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_find_by_name";
    }

    @Override
    @NotNull
    public String description() {
        return "find project by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND BY NAME]");
        System.out.println("ENTER PROJECT NAME");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        printProjects(serviceLocator.getProjectService().findByName(userId, name));
        System.out.println("[OK]");
    }
}
