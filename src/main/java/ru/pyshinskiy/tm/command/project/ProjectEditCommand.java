package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_edit";
    }

    @Override
    @NotNull
    public String description() {
        return "edit existing project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll(currentUser.getId()));
        @NotNull final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        @Nullable final Project project = projectService.findOne(projectId);
        if(project == null) {
            throw new Exception("project doesn't exist");
        }
        @NotNull final Project anotherProject = new Project();
        anotherProject.setUserId(currentUser.getId());
        System.out.println("ENTER NAME");
        anotherProject.setName(terminalService.nextLine());
        anotherProject.setId(project.getId());
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherProject.setStartDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("ENTER FINISH DATE");
        anotherProject.setFinishDate(parseDateFromString(terminalService.nextLine()));
        projectService.merge(anotherProject);
        System.out.println("[OK]");
    }
}
