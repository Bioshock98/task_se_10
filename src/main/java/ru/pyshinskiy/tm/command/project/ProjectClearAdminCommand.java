package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

public final class ProjectClearAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(@Nullable final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    @NotNull
    public String command() {
        return "project_clear_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all users projects";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.removeAttachedTasks();
        projectService.removeAll();
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
