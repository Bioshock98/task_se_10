package ru.pyshinskiy.tm.command.data.load;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;

import java.io.File;

public class DataFasterXmlLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_fasterXml_load";
    }

    @Override
    public @NotNull String description() {
        return "load data by fasterXml from xml format";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.xml");
        savePath.getParentFile().mkdirs();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savePath, Domain.class);
        serviceLocator.getProjectService().persist(domain.getProjects());
        serviceLocator.getTaskService().persist(domain.getTasks());
        serviceLocator.getUserService().persist(domain.getUsers());
        System.out.println("[OK]");
    }
}
