package ru.pyshinskiy.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;
import ru.pyshinskiy.tm.entity.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_bin_load";
    }

    @Override
    @NotNull
    public String description() {
        return "load data from binary format";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User currentUser= serviceLocator.getUserService().getCurrentUser();
        @NotNull final File file = new File(AppConst.SAVE_DIR + "data.bin");
        @NotNull final FileInputStream inputStream = new FileInputStream(file);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getProjectService().persist(domain.getProjects());
        serviceLocator.getTaskService().persist(domain.getTasks());
        serviceLocator.getUserService().persist(domain.getUsers());
        System.out.println("[OK]");
    }
}
