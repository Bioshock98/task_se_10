package ru.pyshinskiy.tm.command.data.save;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;

import java.io.File;

public class DataFasterXmlSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_fasterXml_save";
    }

    @Override
    @NotNull
    public String description() {
        return "save data by fasterXml in xml format";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator);
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.xml");
        savePath.getParentFile().mkdirs();
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(savePath, domain);
        System.out.println("[OK]");
    }
}
