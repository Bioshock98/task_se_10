package ru.pyshinskiy.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;
import ru.pyshinskiy.tm.entity.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class DataBinSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_bin_save";
    }

    @Override
    public @NotNull String description() {
        return "save data in binary format";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator);
        @NotNull final User currentUser= serviceLocator.getUserService().getCurrentUser();
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.bin");
        savePath.getParentFile().mkdirs();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(savePath);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        System.out.println("[OK]");
    }
}
