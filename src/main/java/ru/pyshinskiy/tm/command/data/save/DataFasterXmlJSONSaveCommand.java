package ru.pyshinskiy.tm.command.data.save;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;

import java.io.File;

public class DataFasterXmlJSONSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_fasterXml_json_save";
    }

    @Override
    @NotNull
    public String description() {
        return "save data by fasterXml from json format";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator);
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.json");
        savePath.getParentFile().mkdirs();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(savePath, domain);
        System.out.println("[OK]");
    }
}
