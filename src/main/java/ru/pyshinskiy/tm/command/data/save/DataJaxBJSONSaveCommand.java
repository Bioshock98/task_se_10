package ru.pyshinskiy.tm.command.data.save;

import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;

import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;

public class DataJaxBJSONSaveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_jaxB_json_save";
    }

    @Override
    @NotNull
    public String description() {
        return "save data by jaxB in json format";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator);
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.json");
        savePath.getParentFile().mkdirs();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(savePath);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jc = (JAXBContext) JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, outputStream);
        System.out.println("[OK]");
    }
}
