package ru.pyshinskiy.tm.command.data.load;

import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;

import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.File;

public class DataJaxBXmlLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_jaxB_xml_load";
    }

    @Override
    @NotNull
    public String description() {
        return "load data by jaxB from xml format";
    }

    @Override
    public void execute() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.xml");
        savePath.getParentFile().mkdirs();
        @NotNull final StreamSource streamSource = new StreamSource(savePath);
        @NotNull final JAXBContext jc = (JAXBContext) JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jc.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/xml");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = unmarshaller.unmarshal(streamSource, Domain.class).getValue();
        serviceLocator.getProjectService().persist(domain.getProjects());
        serviceLocator.getTaskService().persist(domain.getTasks());
        serviceLocator.getUserService().persist(domain.getUsers());
        System.out.println("[OK]");
    }
}
