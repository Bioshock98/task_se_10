package ru.pyshinskiy.tm.command.data.load;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.constant.AppConst;
import ru.pyshinskiy.tm.dto.Domain;

import java.io.File;

public class DataFasterXmlJSONLoadCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "data_fasterXml_json_load";
    }

    @Override
    @NotNull
    public String description() {
        return "load data by fasterXml from json format";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final File savePath = new File(AppConst.SAVE_DIR + "data.json");
        savePath.getParentFile().mkdirs();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Domain domain = mapper.readValue(savePath, Domain.class);
        serviceLocator.getProjectService().persist(domain.getProjects());
        serviceLocator.getTaskService().persist(domain.getTasks());
        serviceLocator.getUserService().persist(domain.getUsers());
        System.out.println("[OK]");
    }
}
