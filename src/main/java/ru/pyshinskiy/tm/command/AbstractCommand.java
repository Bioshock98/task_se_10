package ru.pyshinskiy.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.ServiceLocator;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.service.TerminalService;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    @Nullable
    protected TerminalService terminalService;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
    }

    public boolean isAllowed(@Nullable final User user) {
        return user != null;
    }

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;
}
