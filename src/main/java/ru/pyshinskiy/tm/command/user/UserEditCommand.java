package ru.pyshinskiy.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

public final class UserEditCommand extends AbstractCommand {
    
    @Override
    @NotNull
    public String command() {
        return "user_edit";
    }

    @Override
    @NotNull
    public String description() {
        return "edit existing user";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String currentUserPassword = serviceLocator.getUserService().getCurrentUser().getPassword();
        System.out.println("[USER EDIT]");
        System.out.println("ENTER PASSWORD");
        while(!DigestUtils.md5Hex(terminalService.nextLine()).equals(currentUserPassword)) {
            System.out.println("Incorrect password");
        }
        System.out.println("ENTER NEW USERNAME");
        @NotNull final User user = new User();
        user.setId(serviceLocator.getUserService().getCurrentUser().getId());
        user.setLogin(terminalService.nextLine());
        System.out.println("ENTER NEW PASSWORD");
        user.setPassword(terminalService.nextLine());
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }
}
