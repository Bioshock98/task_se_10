package ru.pyshinskiy.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user_change_password";
    }

    @Override
    @NotNull
    public String description() {
        return "change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CHANGE PASSWORD");
        System.out.println("ENTER OLD PASSWORD");
        @NotNull final String userPassword = serviceLocator.getUserService().getCurrentUser().getPassword();
        while(!DigestUtils.md5Hex(terminalService.nextLine()).equals(userPassword)) {
            System.out.println("Incorrect password");
        }
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final User user = new User();
        user.setId(serviceLocator.getUserService().getCurrentUser().getId());
        user.setPassword(terminalService.nextLine());
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }
}
