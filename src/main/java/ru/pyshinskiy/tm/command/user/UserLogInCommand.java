package ru.pyshinskiy.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUser;

public final class UserLogInCommand extends AbstractCommand {
    
    @Override
    public boolean isAllowed(@Nullable final User user) {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "user_log_in";
    }

    @Override
    @NotNull
    public String description() {
        return "log in";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOG IN]");
        System.out.println("ENTER LOGIN");
        @NotNull String login = terminalService.nextLine();
        while(serviceLocator.getUserService().getUserByLogin(login) == null) {
            System.out.println("Login doesn't exist");
            System.out.println("ENTER LOGIN");
            login = terminalService.nextLine();
        }
        System.out.println("[OK]");
        @NotNull User user = new User();
        for(@NotNull User user1 : serviceLocator.getUserService().findAll()) {
            if(login.equals(user1.getLogin())) user = user1;
        }
        printUser(user);
        System.out.println("ENTER PASSWORD");
        @NotNull String password = terminalService.nextLine();
        while(!DigestUtils.md5Hex(password).equals(user.getPassword())) {
            System.out.println("Invalid password");
            System.out.println("ENTER PASSWORD");
            password = terminalService.nextLine();
        }
        serviceLocator.getUserService().setCurrentUser(user);
        System.out.println("[OK]");
    }
}
