package ru.pyshinskiy.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(@Nullable User user) {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "user_create";
    }

    @Override
    @NotNull
    public String description() {
        return "create new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER USERNAME");
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(terminalService.nextLine());
        System.out.println("ENTER PASSWORD");
        user.setPassword(DigestUtils.md5Hex(terminalService.nextLine()));
        serviceLocator.getUserService().persist(user);
        System.out.println("[OK]");
    }
}
