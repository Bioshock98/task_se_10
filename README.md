# Task Manager
## software
+ JRE
+ Java 8
+ Maven 3.0
## developer
Pavel Pyshinskiy

+ email: pavel.pyshinskiy@gmail.com
## build application
```
mvn clean
mvn install
```
## run application
```
java -jar target/artifactId-version-SNAPSHOT.jar
```